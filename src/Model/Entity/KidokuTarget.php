<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * KidokuTarget Entity
 *
 * @property int $id
 * @property int|null $post-id
 * @property int|null $target-user-id
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 */
class KidokuTarget extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'post-id' => true,
        'target-user-id' => true,
        'created' => true,
        'modified' => true,
    ];
}

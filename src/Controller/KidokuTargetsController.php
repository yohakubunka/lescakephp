<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * KidokuTargets Controller
 *
 * @property \App\Model\Table\KidokuTargetsTable $KidokuTargets
 *
 * @method \App\Model\Entity\KidokuTarget[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class KidokuTargetsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $kidokuTargets = $this->paginate($this->KidokuTargets);

        $this->set(compact('kidokuTargets'));
    }

    /**
     * View method
     *
     * @param string|null $id Kidoku Target id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $kidokuTarget = $this->KidokuTargets->get($id, [
            'contain' => [],
        ]);

        $this->set('kidokuTarget', $kidokuTarget);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $kidokuTarget = $this->KidokuTargets->newEntity();
        if ($this->request->is('post')) {
            $kidokuTarget = $this->KidokuTargets->patchEntity($kidokuTarget, $this->request->getData());
            if ($this->KidokuTargets->save($kidokuTarget)) {
                $this->Flash->success(__('The kidoku target has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The kidoku target could not be saved. Please, try again.'));
        }
        $this->set(compact('kidokuTarget'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Kidoku Target id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $kidokuTarget = $this->KidokuTargets->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $kidokuTarget = $this->KidokuTargets->patchEntity($kidokuTarget, $this->request->getData());
            if ($this->KidokuTargets->save($kidokuTarget)) {
                $this->Flash->success(__('The kidoku target has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The kidoku target could not be saved. Please, try again.'));
        }
        $this->set(compact('kidokuTarget'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Kidoku Target id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $kidokuTarget = $this->KidokuTargets->get($id);
        if ($this->KidokuTargets->delete($kidokuTarget)) {
            $this->Flash->success(__('The kidoku target has been deleted.'));
        } else {
            $this->Flash->error(__('The kidoku target could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}

<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Reply $reply
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Replys'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="replys form large-9 medium-8 columns content">
    <?= $this->Form->create($reply) ?>
    <fieldset>
        <legend><?= __('Add Reply') ?></legend>
        <?php
            echo $this->Form->control('source-id');
            echo $this->Form->control('source-type');
            echo $this->Form->control('content');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>

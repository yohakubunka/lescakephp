<ul class="side-nav">
    <li><?= $this->Html->link(__('質問を投稿する'), ['controller'=>'posts', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('質問一覧'), ['controller'=>'posts', 'action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('ユーザー登録'), ['controller'=>'users', 'action' => 'add']) ?></li>
    <li><?= $this->Html->link(__('ログアウト'), ['controller'=>'users', 'action' => 'logout']) ?></li>
  </ul>

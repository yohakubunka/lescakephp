<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\KidokuTarget $kidokuTarget
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Kidoku Targets'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="kidokuTargets form large-9 medium-8 columns content">
    <?= $this->Form->create($kidokuTarget) ?>
    <fieldset>
        <legend><?= __('Add Kidoku Target') ?></legend>
        <?php
            echo $this->Form->control('target-user-id');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>

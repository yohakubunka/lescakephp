<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\KidokuTargetsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\KidokuTargetsTable Test Case
 */
class KidokuTargetsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\KidokuTargetsTable
     */
    public $KidokuTargets;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.KidokuTargets',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('KidokuTargets') ? [] : ['className' => KidokuTargetsTable::class];
        $this->KidokuTargets = TableRegistry::getTableLocator()->get('KidokuTargets', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->KidokuTargets);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
